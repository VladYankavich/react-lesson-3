import "./button-save.css";

const ButtonSave = ({ onClick }) => {
    return (
        <button type="button" className="button-save" onClick={onClick}>Зберегти
            <span>📀</span>
        </button>
    )
}

export default ButtonSave