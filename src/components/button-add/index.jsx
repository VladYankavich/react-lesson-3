import "./button-add.css"

const ButtonAdd = ({onClick}) => {
    return (
        <button type="button" className="button-add" onClick={onClick}>Додати
            <span className="plus">+</span>
        </button>
    )
}

export default ButtonAdd