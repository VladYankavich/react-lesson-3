// import { Component } from "react";
import ButtonSave from "../button-save";

import "./add-items.css";

const AddItems = ({ classInputName, classInputTextName, onSubmit, onInputDataChange, value, onClick }) => {
    return (
        <form className={classInputName}
            onSubmit={onSubmit}
        >
            <input type="text" className={classInputTextName}
                placeholder="що потрібно купити"
                onChange={onInputDataChange}
                value={value}
            />
            <ButtonSave onClick={onClick} />
        </form>
    )
}

export default AddItems

// export default class AddItems extends Component {
//     state = {
//         inputData: ""
//     };

//     onInputDataChange = (event) => {
//         this.setState({
//             inputData: event.target.value
//         });
//     };

//     onSubmit = (event) => {
//         event.preventDefault();
//         this.props.onItemAddet(this.state.inputData);
//         this.setState({
//             inputData: ""
//         });
//     };

//     render() {
//         const { classInputName, classInputTextName, eventFN } = this.props;
//         return (
//             <form className={classInputName}
//                 onSubmit={this.onSubmit}
//             >
//                 <input type="text" className={classInputTextName}
//                     placeholder="що потрібно купити"
//                     onChange={this.onInputDataChange}
//                     value={this.state.inputData}
//                 />
//                 <ButtonSave eventFN={eventFN} />
//             </form>
//         )
//     }
// }