import "./buttons-list.css";

const ButtonsList = ({onClickButtonEdit, onClickButtonDelete, onClickButtonDone, id}) => {
    return (
        <div className="buttons">
            <button id={id} className="button-item" onClick={onClickButtonEdit}>✍</button>
            <button id={id} className="button-item" onClick={onClickButtonDelete}>❌</button>
            <button id={id} className="button-item" onClick={onClickButtonDone}>✅</button>
        </div>
    )
}
 
export default ButtonsList