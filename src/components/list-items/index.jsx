import ButtonsList from "../buttons-list";

import "./list-items.css";

const ListItems = ({ data, classListName, classListItemName,
    onClickButtonEdit, onClickButtonDelete, onClickButtonDone }) => {
    const products = data.map((item) => {
        const { value, done, id} = item;

        let classProductName = "product-name"

        if (done) {
            classProductName += " done"
        }

        return (
            <li key={id} className={classListItemName}>
                <span className={classProductName}>{value}</span>
                <ButtonsList id={id}
                    onClickButtonEdit={onClickButtonEdit}
                    onClickButtonDelete={onClickButtonDelete}
                    onClickButtonDone={onClickButtonDone}
                />
            </li>
        );
    });

    return (
        <ul className={classListName}>
            {products}
        </ul>
    )
}

export default ListItems