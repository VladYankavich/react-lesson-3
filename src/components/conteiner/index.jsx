import { Component } from "react";
import ListItems from "../list-items";
import AddItems from "../add-items";
import ButtonAdd from "../button-add";

import "./conteiner.css";

export default class Conteiner extends Component {
    getRandomId = () => {
        const min = 1;
        const max = 10000;
        return Math.floor(Math.random() * (max - min) + min);
    };

    state = {
        data: [],
        clickAdd: false,
        clickSave: false,
        remove: false,
        inputData: ""
    };

    createItem(value) {
        return {
            value,
            done: false,
            id: this.getRandomId()
        };
    };

    addItem = (text) => {
        const newItem = this.createItem(text);

        this.setState(({ data }) => {
            const newArray = [
                ...data,
                newItem
            ];

            return {
                data: newArray,
                clickSave: true,
                remove: false,
                inputData: ""
            };
        });
    };

    deleteItem = (btn) => {
        this.setState(({ data }) => {
            const idIndex = data.findIndex((el) => el.id === parseInt(btn.target.id));

            const newArray = [
                ...data.slice(0, idIndex),
                ...data.slice(idIndex + 1)
            ];

            if (newArray.length === 0) {
                return {
                    data: newArray,
                    remove: true
                };
            };
            return {
                data: newArray
            };
        });
    };

    editItem = (btn) => {
        this.setState(({ data }) => {
            const idIndex = data.findIndex((el) => el.id === parseInt(btn.target.id));
            const value = data[idIndex].value;
            this.deleteItem(btn)
            return {
                inputData: value
            };
        });
    };

    onToggleDone = (btn) => {
        this.setState(({ data }) => {
            const idIndex = data.findIndex((el) => el.id === parseInt(btn.target.id));
            const oldItem = data[idIndex];
            const newItem = { ...oldItem, done: !oldItem.done };

            const newArray = [
                ...data.slice(0, idIndex),
                newItem,
                ...data.slice(idIndex + 1)
            ];

            return {
                data: newArray
            };
        });
    };

    onInputDataChange = (event) => {
        this.setState({
            inputData: event.target.value
        });
    };

    onSubmit = (event) => {
        event.preventDefault();
        this.addItem(this.state.inputData);
    };

    onClickButtonAdd = () => {
        this.setState(({ clickAdd }) => {
            return {
                clickAdd: !clickAdd
            };
        });
    };

    onClickButtonSave = () => {
        if (this.state.inputData === "") {
            alert("Введіть назву покупки")
        } else {
            this.addItem(this.state.inputData)
            this.setState(() => {
                return {
                    clickSave: true
                };
            });
        };

    };

    onClickButtonDone = () => {
        this.setState(({ done }) => {
            return {
                done: !done
            };
        });
    };

    render() {
        let { clickAdd, clickSave, remove, data } = this.state;
        let classInputName = "input";
        let classInputTextName = "input-text";
        let classListName = "list";
        let classListItemName = "list-item";

        if (clickAdd) {
            classInputName += " active";
        };

        if (clickSave) {
            classListName += " active";
        };

        if (remove) {
            classListName = "list";
        };

        return (
            <div className="conteiner">
                <ListItems classListName={classListName}
                    classListItemName={classListItemName}

                    data={data}
                    onClickButtonEdit={this.editItem}
                    onClickButtonDelete={this.deleteItem}
                    onClickButtonDone={this.onToggleDone}
                />
                <AddItems classInputName={classInputName}
                    classInputTextName={classInputTextName}
                    onSubmit={this.onSubmit}
                    onInputDataChange={this.onInputDataChange}
                    value={this.state.inputData}
                    onClick={this.onClickButtonSave}
                />
                <ButtonAdd onClick={this.onClickButtonAdd} />
            </div>
        );
    };
};

